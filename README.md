# E-Commerce-Java

#### 介绍
电商项目。
前后端分离，后端服务介绍如下，前端介绍参考E-Commerce-Web目录下README.md文件。
### 部署环境
JDK、MySQL、Redis

### 服务启动
1. 使用CodeArts IDE for java打开项目后，修改application.yml中的配置数据库、Redis连接信息；
2. 参考db_init_test.sql创建数据库e-commerce；
3. 点击编辑配置，选择CommerceApplication，配置VM options参数（--add-opens java.base/sun.nio.ch=ALL-UNNAMED）。
![img.png](images/img.png)
![img.png](images/img2.png)
![img.png](images/img3.png)

4. 运行CommerceApplication启动类运行后端服务。

### 接口介绍
#### Top10商品获取接口
1. /api/top/

#### 用户接口

1.  注册
2.  登录
3.  登出
#### 商品接口

1. 商品列表
2. 商品详情
#### 购物车解耦

1. 购物车列表
2. 加入购物车
3. 移除商品
4. 结算
#### 订单接口

1.  历史订单列表

