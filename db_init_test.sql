SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
CREATE TABLE `cart` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int DEFAULT NULL,
  `commodity_id` int DEFAULT NULL,
  `commodity_count` int DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

insert into `cart`(`id`,`uid`,`commodity_id`,`commodity_count`,`update_time`) values
('3','1','2','1','2024-12-03 17:16:21'),
('4','2','3','2','2024-12-03 17:16:21'),
('5','1','13','1','2024-12-03 17:16:21'),
('6','2','14','3','2024-12-03 17:16:21'),
('7','2','22','2','2024-12-03 17:16:21'),
('8','1','31','2','2024-12-03 17:16:21'),
('9','2','11','4','2024-12-03 17:16:21'),
('10','2','33','1','2024-12-03 17:16:21'),
('11','2','25','1','2024-12-03 17:16:21');

-- ----------------------------
-- Table structure for commodity
-- ----------------------------
CREATE TABLE `commodity` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `category` int NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `price` float NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `picture_urls` json DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

insert into `commodity`(`id`,`name`,`category`,`title`,`price`,`desc`,`picture_urls`) values
('1','西红柿','1','优质西红柿','3.5','保质保鲜','["https://test.com/photo1", "https://test.com/photo12"]'),
('2','苹果','2','优质苹果','0.4','保质保鲜','["https://test.com/photo2", "https://test.com/photo22"]'),
('3','西兰花','1','优质西兰花','2.2','保质保鲜','["https://test.com/photo3", "https://test.com/photo32"]'),
('4','香蕉','2','优质香蕉','3.3','保质保鲜','["https://test.com/photo4", "https://test.com/photo42"]'),
('5','胡萝卜','1','优质胡萝卜','8.3','保质保鲜','["https://test.com/photo5", "https://test.com/photo52"]'),
('6','樱桃','2','优质樱桃','7.4','保质保鲜','["https://test.com/photo6", "https://test.com/photo62"]'),
('7','黄瓜','1','优质黄瓜','3.9','保质保鲜','["https://test.com/photo7", "https://test.com/photo72"]'),
('8','枣','2','优质枣','7.4','保质保鲜','["https://test.com/photo8", "https://test.com/photo82"]'),
('9','绿豆角','1','优质绿豆角','3.5','保质保鲜','["https://test.com/photo9", "https://test.com/photo92"]'),
('10','接骨木莓','2','优质接骨木莓','9.1','保质保鲜','["https://test.com/photo10", "https://test.com/photo102"]'),
('11','茄子','1','优质茄子','3.4','保质保鲜','["https://test.com/photo11", "https://test.com/photo112"]'),
('12','无花果','2','优质无花果','4.6','保质保鲜','["https://test.com/photo12", "https://test.com/photo122"]'),
('13','生菜','1','优质生菜','3.1','保质保鲜','["https://test.com/photo13", "https://test.com/photo132"]'),
('14','葡萄','2','优质葡萄','5.6','保质保鲜','["https://test.com/photo14", "https://test.com/photo142"]'),
('15','蘑菇','1','优质蘑菇','7.5','保质保鲜','["https://test.com/photo15", "https://test.com/photo152"]'),
('16','蜜瓜','2','优质蜜瓜','1.1','保质保鲜','["https://test.com/photo16", "https://test.com/photo162"]'),
('17','洋葱','1','优质洋葱','4.0','保质保鲜','["https://test.com/photo17", "https://test.com/photo172"]'),
('18','奇异果','2','优质奇异果','0.4','保质保鲜','["https://test.com/photo18", "https://test.com/photo182"]'),
('19','辣椒','1','优质辣椒','3.9','保质保鲜','["https://test.com/photo19", "https://test.com/photo192"]'),
('20','柠檬','2','优质柠檬','8.6','保质保鲜','["https://test.com/photo20", "https://test.com/photo202"]'),
('21','菠萝','1','优质菠萝','4.1','保质保鲜','["https://test.com/photo21", "https://test.com/photo212"]'),
('22','芒果','2','优质芒果','1.5','保质保鲜','["https://test.com/photo22", "https://test.com/photo222"]'),
('23','石榴','1','优质石榴','2.8','保质保鲜','["https://test.com/photo23", "https://test.com/photo232"]'),
('24','油桃','2','优质油桃','7.5','保质保鲜','["https://test.com/photo24", "https://test.com/photo242"]'),
('25','南瓜','1','优质南瓜','0.2','保质保鲜','["https://test.com/photo25", "https://test.com/photo252"]'),
('26','橙子','2','优质橙子','0.6','保质保鲜','["https://test.com/photo26", "https://test.com/photo262"]'),
('27','萝卜','1','优质萝卜','2.6','保质保鲜','["https://test.com/photo27", "https://test.com/photo272"]'),
('28','木瓜','2','优质木瓜','1.3','保质保鲜','["https://test.com/photo28", "https://test.com/photo282"]'),
('29','菠菜','1','优质菠菜','9.6','保质保鲜','["https://test.com/photo29", "https://test.com/photo292"]'),
('30','棠梨','2','优质棠梨','1.5','保质保鲜','["https://test.com/photo30", "https://test.com/photo302"]'),
('31','娃娃菜','1','优质娃娃菜','1.6','保质保鲜','["https://test.com/photo31", "https://test.com/photo312"]'),
('32','树莓','2','优质树莓','3.8','保质保鲜','["https://test.com/photo32", "https://test.com/photo322"]'),
('33','红薯','1','优质红薯','2.3','保质保鲜','["https://test.com/photo33", "https://test.com/photo332"]'),
('34','草莓','2','优质草莓','5.9','保质保鲜','["https://test.com/photo34", "https://test.com/photo342"]'),
('35','芜菁','1','优质芜菁','1.9','保质保鲜','["https://test.com/photo35", "https://test.com/photo352"]'),
('36','番茄','2','优质番茄','10.0','保质保鲜','["https://test.com/photo36", "https://test.com/photo362"]'),
('37','西瓜','1','优质西瓜','9.3','保质保鲜','["https://test.com/photo37", "https://test.com/photo372"]'),
('38','牛油果','2','优质牛油果','8.1','保质保鲜','["https://test.com/photo38", "https://test.com/photo382"]'),
('39','西葫芦','1','优质西葫芦','2.6','保质保鲜','["https://test.com/photo39", "https://test.com/photo392"]'),
('40','蓝莓','2','优质蓝莓','8.1','保质保鲜','["https://test.com/photo40", "https://test.com/photo402"]');

-- ----------------------------
-- Table structure for order
-- ----------------------------
CREATE TABLE `order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int DEFAULT NULL,
  `order_time` datetime NOT NULL,
  `commodity_list` json DEFAULT NULL,
  `total_price` float NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

insert into `order`(`id`,`uid`,`order_time`,`commodity_list`,`total_price`) values
('3','2','2024-12-04 17:05:49','[{"name": "石榴", "count": 3, "price": 2.8}, {"name": "柠檬", "count": 1, "price": 8.6}]','17.0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

insert into `user`(`id`,`account`,`password`,`name`) values
('2','18739442562','244d8fe1cb99016a242d1b1b7eb5e9b8','jack');

CREATE TABLE `tops` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '品类',
  `clickCount` bigint unsigned DEFAULT NULL COMMENT '点击总数',
  `orderCount` bigint unsigned DEFAULT NULL COMMENT '下单总数',
  `paymentCount` bigint unsigned DEFAULT NULL COMMENT '支付总数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
