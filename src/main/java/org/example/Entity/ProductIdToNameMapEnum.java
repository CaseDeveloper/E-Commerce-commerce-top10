
package org.example.Entity;

import java.util.HashMap;
import java.util.Map;

public enum ProductIdToNameMapEnum {
    INSTANCE;

    private final Map<String, String> productIdToNameMap;

    ProductIdToNameMapEnum() {
        productIdToNameMap = new HashMap<>();
        productIdToNameMap.put("1", "智能手机");
        productIdToNameMap.put("2", "护肤品套装");
        productIdToNameMap.put("3", "运动鞋");
        productIdToNameMap.put("4", "平板电脑");
        productIdToNameMap.put("5", "休闲食品");
        productIdToNameMap.put("6", "智能手环");
        productIdToNameMap.put("7", "运动水杯");
        productIdToNameMap.put("8", "儿童玩具");
        productIdToNameMap.put("9", "营养补充剂");
        productIdToNameMap.put("10", "家用小电器");
        productIdToNameMap.put("11", "时尚外套");
        productIdToNameMap.put("12", "蓝牙耳机");
        productIdToNameMap.put("13", "创意家居饰品");
        productIdToNameMap.put("14", "车载香水");
        productIdToNameMap.put("15", "智能手表");
        productIdToNameMap.put("16", "美妆工具");
        productIdToNameMap.put("17", "电子游戏");
        productIdToNameMap.put("18", "户外背包");
        productIdToNameMap.put("19", "宠物玩具");
        productIdToNameMap.put("20", "健身器材");
    }

    public Map<String, String> getProductIdToNameMap() {
        return productIdToNameMap;
    }
}

