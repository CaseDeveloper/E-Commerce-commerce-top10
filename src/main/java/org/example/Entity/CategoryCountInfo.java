package org.example.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryCountInfo implements Serializable {
    private String categoryId;
    private long clickCount;
    private long orderCount;
    private long payCount;

}