package org.example.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.utils.GsonUtil;

/**
 * 个人订单详情，用于数据传输
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonnelCart {
    //商品id
    private Integer id;
    //商品名称
    private String name;
    //商品分类,1-蔬菜；2-水果
    private Integer category;
    //标题
    private String title;
    //商品描述
    private String desc;
    //价格
    private Float price;
    //商品数量
    private Integer count;
    //商品图片--json
    private String[] picture_urls;

    public void setPicture_urls(String picture_urls) {
        this.picture_urls = (String[]) GsonUtil.jsonToArray(picture_urls,String[].class);
    }
}
