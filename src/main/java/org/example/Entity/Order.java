package org.example.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.utils.GsonUtil;
import java.sql.Date;
import java.util.List;

/**
 * 订单表
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("uid")
public class Order {
    private Integer id;
    //用户id
    private Integer uid;
    //商品对象列表--json
    private List<CommodityOrder> commodity_list;
    //订单总价格
    private Float total_price;
    //订单生成时间
    private Date order_time;

    public void setCommodity_list(String commodity_list) {
        this.commodity_list = (List<CommodityOrder>)GsonUtil.jsonToObject(commodity_list, List.class);
    }
}
