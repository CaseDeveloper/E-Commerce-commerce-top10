package org.example.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.utils.GsonUtil;

/**
 * 商品表
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Commodity {
    private Integer id;
    //商品名称
    private String name;
    //商品分类,1-蔬菜；2-水果
    private Integer category;
    //标题
    private String title;
    //价格
    private Float price;
    //商品描述
    private String desc;
    //商品图片--json
    private String[] picture_urls;

    public void setPicture_urls(String picture_urls) {
        this.picture_urls = (String[]) GsonUtil.jsonToArray(picture_urls,String[].class);
    }
}
