package org.example.Entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommodityMapRecognition {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String picture_path;

    private String predicted_label;

    List<List<String>> scores;
}
