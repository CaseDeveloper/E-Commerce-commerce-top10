
package org.example.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusinessData {
    // 品类，使用String类型来存储品类名称等信息
    private String category;
    // 点击总数，使用int类型统计点击次数
    private long clickCount;
    // 下单总数，使用int类型统计下单次数
    private long orderCount;
    // 支付总数，使用int类型统计支付次数
    private long paymentCount;
}

