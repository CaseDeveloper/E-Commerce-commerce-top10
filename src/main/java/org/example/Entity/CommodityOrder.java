package org.example.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 订单商品
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommodityOrder {
    private String name;
    private Integer count;
    private Float price;
}
