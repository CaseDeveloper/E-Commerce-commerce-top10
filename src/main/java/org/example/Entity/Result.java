package org.example.Entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    private Integer code;//业务状态码  2000-成功  其他-失败
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String msg;//提示信息
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;//响应数据

    //快速返回操作成功响应结果数据
    public static <E> Result<E> success(E data) {
        return new Result<>(200, "OK", data);
    }
    //快速返回操作成功响应操作结果&数据
    public static <E> Result<E> success(String msg,E data) {
        return new Result<>(200, msg, data);
    }
 
    //快速返回操作成功响应结果
    public static Result success() {
        return new Result(200, "OK", null);
    }
 
    public static Result error(String message) {
        return new Result(1, message, null);
    }

    //未登录
    public static Result noLogin(String message) {
        return new Result(401, message, null);
    }
}