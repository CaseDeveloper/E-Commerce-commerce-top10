package org.example.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

/**
 * 购物车表
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cart {
    private Integer id;
    //用户id
    private Integer uid;
    //购物车中商品id
    private Integer commodity_id;
    //购物车中商品数量
    private Integer commodity_count;
    //加入购物车时间
    private Date update_time;
}
