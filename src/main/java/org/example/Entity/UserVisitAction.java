
package org.example.Entity;
// 用户访问动作表对应的Java类

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVisitAction {
    private String date;
    private long user_id;
    private String session_id;
    private long page_id;
    private String action_time;
    private String search_keyword;
    private long click_category_id;
    private long click_product_id;
    private String order_category_ids;
    private String order_product_ids;
    private String pay_category_ids;
    private String pay_product_ids;
    private long city_id;


}