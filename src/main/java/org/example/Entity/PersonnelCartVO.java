package org.example.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页查询对象
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonnelCartVO {
    private Integer total;
    private List<PersonnelCart> data;
}
