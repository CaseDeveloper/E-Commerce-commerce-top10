package org.example.test;


import java.text.DecimalFormat;
import java.util.Random;

public class Demo1 {
    public static void main(String[] args) {
        String username = "1234566654321";
        System.out.println(username.substring(0,5));
        System.out.println(username.substring(username.length()-4));

        String dbname = "";

        String[] f2 = {
                "苹果", "香蕉", "樱桃", "枣", "接骨木莓", // 注意：接骨木莓在中文中可能不常见，这里为了对应英文原数组而保留
                "无花果", "葡萄", "蜜瓜", "奇异果", "柠檬",
                "芒果", "油桃", "橙子", "木瓜", "棠梨", // 注意：Quince在中文中没有直接对应的翻译，这里用棠梨代替，但棠梨并不是Quince的直译
                "树莓", "草莓", "番茄", "牛油果", "蓝莓"};
        int fn1 = 0;
        String[] f1 = {
                "西兰花", "胡萝卜", "黄瓜", "绿豆角", "茄子",
                "生菜", "蘑菇", "洋葱", "辣椒", "菠萝",
                "石榴", "南瓜", "萝卜", "菠菜","娃娃菜",
                "红薯", "芜菁", "西瓜", "西葫芦"
        };
        int fn2 = 0;
        Random random = new Random();
        for (int i = 2; i <= 40; i++) {

            // 使用DecimalFormat格式化浮点数，只保留一位小数
            DecimalFormat df = new DecimalFormat("#.0");
            float price = Float.parseFloat(df.format(random.nextFloat()*10));

            if (i%2==0){
                System.out.println("("+i+",\""+f2[fn2]+"\","+2+",\"优质"+f2[fn2++]+"\","+price+",\"保质保鲜\",'[\"https://test.com/photo"+i+"\",\"https://test.com/photo"+i+"2\"]'),");
            }else{
                System.out.println("("+i+",\""+f1[fn1]+"\","+1+",\"优质"+f1[fn1++]+"\","+price+",\"保质保鲜\",'[\"https://test.com/photo"+i+"\",\"https://test.com/photo"+i+"2\"]'),");
            }
        }

    }
}
