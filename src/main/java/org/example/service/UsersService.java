package org.example.service;

import org.example.Entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UsersService {

    void register(User users);

    User findByAccount(String account);
}
