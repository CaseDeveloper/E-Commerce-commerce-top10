package org.example.service;

import org.example.Entity.Order;

import java.util.List;

public interface OrderService {
    List<Order> list(Integer uid, Integer page_number, Integer page_size);

    int getTotal(Integer uid);
}
