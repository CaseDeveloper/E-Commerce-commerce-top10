package org.example.service;

import org.example.Entity.BusinessData;

import java.util.List;

public interface BusinessDataService {
    List<BusinessData> getTop10();
}
