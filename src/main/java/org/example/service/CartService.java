package org.example.service;

import org.example.Entity.PersonnelCart;

import java.util.List;

public interface CartService {
    List<PersonnelCart> list(String account, String keyword, Integer page_number, Integer page_size);

    Integer add(Integer uid, Integer commodity_id, Integer commodity_count);

    Integer delete(Integer uid, Integer commodity_id);

    Integer pay(Integer uid, List<Integer> commodity_ids);

    int getTotal(String account,String keyword);
}
