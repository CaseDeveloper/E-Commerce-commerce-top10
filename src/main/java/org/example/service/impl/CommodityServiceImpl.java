package org.example.service.impl;

import org.example.Entity.Commodity;
import org.example.mapper.CommodityMapper;
import org.example.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommodityServiceImpl implements CommodityService {

    @Autowired
    private CommodityMapper commodityMapper;

    //分页查询商品列表
    @Override
    public List<Commodity> list(String keyword, Integer page_number, Integer page_size) {
        int offset = (page_number - 1)*page_size;

        if (keyword == null || "".equals(keyword)){
            //1.查询所有
            return commodityMapper.findCommodityList(offset,page_size);
        }else {
            //2.模糊查询
            return commodityMapper.findCommodityByKeyword(keyword,offset,page_size);
        }
    }

    //查询商品详情
    @Override
    public Commodity findCommodityById(Integer id) {

        return commodityMapper.findCommodityById(id);
    }

    @Override
    public int getTotal(String keyword) {
        if (keyword == null || "".equals(keyword)){
            //1.查询所有
            return commodityMapper.getTotal();
        }else {
            //2.模糊查询
            return commodityMapper.getTotal2(keyword);
        }
    }
}
