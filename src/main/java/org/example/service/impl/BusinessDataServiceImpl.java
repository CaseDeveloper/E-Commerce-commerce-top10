package org.example.service.impl;

import org.example.Entity.BusinessData;
import org.example.mapper.BusinessDataMapper;
import org.example.service.BusinessDataService;
import org.example.utils.Top10;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessDataServiceImpl implements BusinessDataService {

    @Autowired
    private BusinessDataMapper businessDataMapper;

    @Override
    public List<BusinessData> getTop10() {
        //1.查询数据库
        List<BusinessData> list = businessDataMapper.top10();
        //2.数据库为空执行top10方法&插入数据库
        if (list.size() > 0){
            return list;
        }else {
            //2.1 清空表
            int temp = businessDataMapper.deleteAll();
            //2.2 获取top10数据
            List<BusinessData> list1 = Top10.top10();
            //2.3 插入表
            for (BusinessData businessData : list1) {
                businessDataMapper.insertTop(businessData);
            }
            return list1;

        }
    }
}
