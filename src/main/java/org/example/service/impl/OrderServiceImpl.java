package org.example.service.impl;

import org.example.Entity.Order;
import org.example.mapper.OrderMapper;
import org.example.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> list(Integer uid, Integer page_number, Integer page_size) {
        int offset = (page_number - 1)*page_size;
        return orderMapper.listByUid(uid,offset,page_size);
    }

    @Override
    public int getTotal(Integer uid) {
        return orderMapper.getTotal(uid);
    }
}
