package org.example.service.impl;

import org.example.Entity.User;
import org.example.mapper.UsersMapper;
import org.example.service.UsersService;
import org.example.utils.Md5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersServiceImpl implements UsersService {


    @Autowired
    private UsersMapper usersMapper;

    @Override
    public User findByAccount(String account) {
        return usersMapper.findByAccount(account);
    }

    @Override
    public void register(User user) {
        //加密
        String md5Password = Md5Util.getMD5String(user.getPassword());
        //添加
        usersMapper.register(user.getAccount(), md5Password, user.getName());

    }
}
