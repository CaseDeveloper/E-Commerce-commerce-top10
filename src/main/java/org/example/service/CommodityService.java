package org.example.service;

import org.example.Entity.Commodity;

import java.util.List;

public interface CommodityService {

    List<Commodity> list(String keyword,Integer page_number,Integer page_size);

    Commodity findCommodityById(Integer id);

    int getTotal(String keyword);
}
