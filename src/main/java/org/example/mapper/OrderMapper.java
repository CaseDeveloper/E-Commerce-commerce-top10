package org.example.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.Entity.Order;

import java.util.List;

@Mapper
public interface OrderMapper {
    @Insert("INSERT INTO `order`(`uid`,`commodity_list`, `total_price`, `order_time`) values(#{uid},#{commodityStr},#{total_price},now())")
    Integer addOrder(Integer uid,String commodityStr, float total_price);

    @Select("select * from `order` where uid=#{uid} LIMIT #{offset}, #{limit}")
    List<Order> listByUid(Integer uid, int offset, Integer limit);

    @Select("select count(1) from `order` where uid=#{uid}")
    int getTotal(Integer uid);
}
