package org.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.Entity.Commodity;


import java.util.List;

@Mapper
public interface CommodityMapper {
 
    @Select("SELECT * FROM commodity where name like CONCAT('%', #{keyword}, '%') LIMIT #{offset}, #{limit}")
    List<Commodity> findCommodityByKeyword(String keyword, int offset, int limit);


    @Select("SELECT * FROM commodity LIMIT #{offset}, #{limit}")
    List<Commodity> findCommodityList(int offset, int limit);

    @Select("SELECT * FROM commodity where id = #{id}")
    Commodity findCommodityById(Integer id);
    @Select("select count(1) from commodity")
    int getTotal();
    @Select("select count(1) from commodity where name like CONCAT('%', #{keyword}, '%')")
    int getTotal2(String keyword);
}