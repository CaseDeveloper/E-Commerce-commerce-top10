package org.example.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.Entity.User;

@Mapper
public interface UsersMapper {
 
    @Select("select * from user where account = #{account}")
    User findByAccount(String account);
 
    @Insert("insert into user(account,password,name)" +
            "values(#{account},#{md5Password},#{name})")
    void register(String account, String md5Password, String name);
}