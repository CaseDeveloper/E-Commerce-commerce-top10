package org.example.mapper;

import org.apache.ibatis.annotations.*;
import org.example.Entity.Cart;
import org.example.Entity.PersonnelCart;

import java.util.List;

@Mapper
public interface CartMapper {
    @Select("SELECT cd.*,c.commodity_count count from cart AS c ,user AS u,commodity AS cd " +
            "where c.uid=u.id and c.commodity_id =cd.id and u.account = #{account} LIMIT #{offset}, #{limit}")
    List<PersonnelCart> findList(String account, Integer offset, Integer limit);
    @Select("SELECT cd.*,c.commodity_count count from cart AS c ,user AS u,commodity AS cd "+
            "where c.uid=u.id and c.commodity_id =cd.id and u.account =  #{account} and cd.name like CONCAT('%', #{keyword}, '%') LIMIT #{offset}, #{limit}")
    List<PersonnelCart> findByKeyword(String account, String keyword, Integer offset, Integer limit);

    @Select("SELECT * from cart where uid=#{uid} and commodity_id=#{commodity_id}")
    Cart findByCommodity(Integer uid, Integer commodity_id);

    @Update("update cart set commodity_count=commodity_count + #{commodity_count} where uid=#{uid} and commodity_id=#{commodity_id}")
    Integer updateCart(Integer uid, Integer commodity_id, Integer commodity_count);

    @Insert("insert into cart(uid,commodity_id,commodity_count,update_time) values(#{uid},#{commodity_id},#{commodity_count},now())")
    Integer addCart(Integer uid, Integer commodity_id, Integer commodity_count);

    @Delete("delete from cart where uid=#{uid} and commodity_id=#{commodity_id}")
    Integer deleteCart(Integer uid, Integer commodity_id);

    @Select("SELECT count(1) from cart AS c ,user AS u " +
            "where c.uid=u.id and u.account = #{account}")
    int getTotal(String account);

    @Select("SELECT count(1) from cart AS c ,user AS u,commodity AS cd "+
            "where c.uid=u.id and c.commodity_id =cd.id and u.account =  #{account} and cd.name like CONCAT('%', #{keyword}, '%') ")
    int getTotal2(String account,String keyword);
}
