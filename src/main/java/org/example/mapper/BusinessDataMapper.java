package org.example.mapper;


import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.Entity.BusinessData;
import java.util.List;

@Mapper
public interface BusinessDataMapper {


    @Delete("delete from tops ")
    int deleteAll();

    @Select("select * from tops limit 0,10 ")
    List<BusinessData> top10();

    @Insert("insert into tops(category,clickCount,orderCount,paymentCount) values(#{category},#{clickCount},#{orderCount},#{paymentCount})")
    int insertTop(BusinessData businessData);
}
