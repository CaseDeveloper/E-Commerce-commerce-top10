package org.example.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexValidator {

    public static boolean validate(String input, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }
}
