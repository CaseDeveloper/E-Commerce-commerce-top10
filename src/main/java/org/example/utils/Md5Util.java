package org.example.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class Md5Util {

	private static String key = "keuyiadef2341jdioe12io";

	/**
	 * MD5加密方法
	 *
	 * @param text 明文
	 * @return 密文
	 * @throws Exception
	 */
	public static String getMD5String(String text) {
		// 加密后的字符串
		String encodeStr = DigestUtils.md5Hex(text + key);
		return encodeStr;
	}

	/**
	 * MD5验证方法
	 *
	 * @param text 明文
	 * @param md5  密文
	 * @return true
	 * @throws Exception
	 */
	public static boolean verify(String text, String md5){
		// 根据传入的密钥进行验证
		String md5Text = getMD5String(text);
		if (md5Text.equalsIgnoreCase(md5)) {
			System.out.println("MD5验证通过！");
			return true;
		}
		return false;
	}

	// 测试方法
	public static void main(String[] args) {
		// 注册：用户名tom 密码123456
		// 添加用户时，进行加密
		String testString = Md5Util.getMD5String("123456");
		System.out.println("经MD5加密后的字符串为：" + testString);

		// 登录：用户名tom 密码123456     select * from user where username = tom and password = 123456
		// 根据用户名进行查询 f00485441dfb815c75a13f3c3389c0b9
		boolean verify = Md5Util.verify("123456",  testString);
		System.out.println(verify);
	}
}
