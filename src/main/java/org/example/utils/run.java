
package org.example.utils;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.example.Entity.BusinessData;

import java.util.List;


public class run {
    public static void main(String[] args) {
          SparkConf conf = new SparkConf().setAppName("Top10").setMaster("local[*]");
        // 创建JavaSparkContext，该对象是提交的入口
        JavaSparkContext sc = new JavaSparkContext(conf);
        Top10 top10 = new Top10();
        List<BusinessData> sparkRun = top10.sparkRun(sc);
        for (BusinessData businessData : sparkRun) {
            System.out.println(businessData);
        }
        // 9. 关闭连接
        sc.stop();
    }
}
