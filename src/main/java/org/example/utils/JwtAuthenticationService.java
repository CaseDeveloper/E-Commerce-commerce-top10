package org.example.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Map;

public class JwtAuthenticationService {

    public String getUsernameFromToken(String token) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getClaim("username").asString();
    }

    public static void main(String[] args) {
        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGFpbXMiOnsiaWQiOjEsInVzZXJuYW1lIjoiZ3VvZ2FvanUifSwiZXhwIjoxNzE2NzU3MjMyfQ.v_jFTfARCi7vdEynrD0iAYwOVrxeS_bB0_6P8c1P-gY";
        
        JwtAuthenticationService authenticationService = new JwtAuthenticationService();
        String username = authenticationService.getUsernameFromToken(token);
        
        System.out.println("Username: " + username);

        Map<String, Object> map = JWT.require(Algorithm.HMAC256("wal"))
                .build()
                .verify(token)
                .getClaim("claims")
                .asMap();
        if (map != null){
            for (String k : map.keySet()) {
                System.out.println(k+"------"+map.get(k));
            }
        }

    }
}
