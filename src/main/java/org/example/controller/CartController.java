package org.example.controller;

import org.example.Entity.PersonnelCart;
import org.example.Entity.PersonnelCartVO;
import org.example.Entity.Result;
import org.example.Entity.User;
import org.example.service.CartService;
import org.example.service.UsersService;
import org.example.utils.GsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 购物车API
 */
@Validated
@RestController
@RequestMapping("/api/cart")
public class CartController {

    @Autowired
    private CartService cartService;
    @Autowired
    private UsersService usersService;

    @Autowired
    private RedisTemplate redisTemplate;

    @PostMapping("/list")
    public Result<PersonnelCartVO> list(@RequestBody String paramStr,@RequestHeader(name = "Authorization") String token){
        //1.获取用户信息account
        String account = (String) redisTemplate.opsForValue().get(token);
        //2.分页查询购物车列表
        Map<String, Object> paramMap = GsonUtil.jsonToMap(paramStr);
        List<PersonnelCart> list = cartService.list(account,(String) paramMap.get("keyword"), (Integer) paramMap.get("page_number"), (Integer) paramMap.get("page_size"));
        int total = cartService.getTotal(account,(String) paramMap.get("keyword"));
        //3.返回结果
        return Result.success(new PersonnelCartVO(total,list));
    }

    @PostMapping("/")
    public Result<String> add(@RequestBody String paramStr,@RequestHeader(name = "Authorization") String token){
        //1.获取用户信息
        String account = (String) redisTemplate.opsForValue().get(token);
        //查询用户
        User loginUser = usersService.findByAccount(account);

        //2.加入购物车
        Map<String, Object> paramMap = GsonUtil.jsonToMap(paramStr);
        Integer tmp = cartService.add(loginUser.getId(),(Integer)paramMap.get("commodity_id"), (Integer)paramMap.get("commodity_count"));

        //3.返回结果
        if (tmp > 0){
            return Result.success();
        }else {
            return Result.error("添加购物车失败！");
        }

    }
    @DeleteMapping("/")
    public Result<String> delete(@RequestBody String paramStr,@RequestHeader(name = "Authorization") String token){
        //1.获取用户信息
        String account = (String) redisTemplate.opsForValue().get(token);
        //查询用户
        User loginUser = usersService.findByAccount(account);

        //2.移出购物车
        Map<String, Object> paramMap = GsonUtil.jsonToMap(paramStr);
        Integer tmp = cartService.delete(loginUser.getId(),(Integer)paramMap.get("commodity_id"));

        //3.返回结果
        if (tmp > 0){
            return Result.success();
        }else {
            return Result.error("移出购物车失败！");
        }

    }

    @PostMapping("/pay")
    public Result<String> pay(@RequestBody String paramStr,@RequestHeader(name = "Authorization") String token){
        //1.获取用户信息
        String account = (String) redisTemplate.opsForValue().get(token);
        //查询用户
        User loginUser = usersService.findByAccount(account);

        //2.新增订单
        //获取商品id列表
        Map<String, Object> paramMap = GsonUtil.jsonToMap(paramStr);
        List<Double> ids =(List<Double>) paramMap.get("commodity_ids");
        List<Integer> commodity_ids = new ArrayList<>();
        Iterator<Double> iterator = ids.iterator();
        while (iterator.hasNext()){
            commodity_ids.add(iterator.next().intValue());
        }
        Integer tmp = cartService.pay(loginUser.getId(),commodity_ids);
        if (tmp > 0){
            return Result.success();
        }else {
            return Result.error("支付失败！");
        }
    }
}
