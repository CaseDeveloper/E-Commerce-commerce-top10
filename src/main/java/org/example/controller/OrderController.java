package org.example.controller;

import org.example.Entity.Order;
import org.example.Entity.OrderVO;
import org.example.Entity.Result;
import org.example.Entity.User;
import org.example.service.OrderService;
import org.example.service.UsersService;
import org.example.utils.GsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 订单API
 */
@Validated
@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UsersService usersService;

    @PostMapping("/list")
    public Result<OrderVO> list(@RequestBody String paramStr, @RequestHeader(name = "Authorization") String token){
        //1.获取用户信息
        String account = (String) redisTemplate.opsForValue().get(token);
        //查询用户
        User loginUser = usersService.findByAccount(account);
        //2.分页查询订单列表
        Map<String, Object> paramMap = GsonUtil.jsonToMap(paramStr);
        List<Order> list = orderService.list(loginUser.getId(), (Integer) paramMap.get("page_number"), (Integer) paramMap.get("page_size"));
        int total = orderService.getTotal(loginUser.getId());
        //3.返回结果
        return Result.success(new OrderVO(total,list));
    }

}
