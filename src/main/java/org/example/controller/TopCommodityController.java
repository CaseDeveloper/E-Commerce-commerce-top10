package org.example.controller;

import org.example.Entity.BusinessData;
import org.example.Entity.BusinessDataVO;
import org.example.Entity.Result;
import org.example.service.BusinessDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * top10商品
 **/
@RestController
@RequestMapping("/api/top")
public class TopCommodityController {

    @Autowired
    private BusinessDataService businessDataService;

    @PostMapping("/")
    public Result<BusinessDataVO> top10(){
        List<BusinessData> list = businessDataService.getTop10();

        return Result.success(new BusinessDataVO(list));
    }
}
